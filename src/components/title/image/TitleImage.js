import { Row, Col } from "reactstrap";
import backgroundImg from "../../../assets/images/background.jpg";

function TitleImage() {
    return (
        <Row className="mt-2">
            <Col>
                <img src={backgroundImg} alt="title" width={500} className="img-thumbnail" />
            </Col>
        </Row>
    )
}

export default TitleImage;