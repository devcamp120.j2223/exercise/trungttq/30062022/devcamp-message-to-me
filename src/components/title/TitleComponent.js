import TitleText from "./text/TitleText";
import TitleImage from "./image/TitleImage";

function TitleComponent() {
    return (
        <div>
            <TitleText />
            <TitleImage />
        </div>
    )
}

export default TitleComponent;